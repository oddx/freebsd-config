PATH="${HOME}/bin:/usr/local/bin/mongod:/usr/local/openjdk8/bin/:${HOME}/.local/bin/:${PATH}"
export EDITOR='vim'
export HISTFILE="${HOME}/.sh_history"
export XDG_CONFIG_HOME=${HOME}/.config/
alias ls='ls -AFGh'
alias vi="${EDITOR}"
alias screenkey="screenkey -p bottom -g 35%x45%+60%+55% --bg-color '#333' --opacity .60 --font-color '#8CAAB2' -f 'Monoid' -t 1"
# alias stack='stack --system-ghc'
set -o emacs
PS1='$(echo "${PWD##*/}") $(echo -e "\e[1;34m")λ$(echo -e "\e[0m") '


if [ -z $DISPLAY ] && [ $(tty) = /dev/ttyv0 ]; then
   startx
fi
